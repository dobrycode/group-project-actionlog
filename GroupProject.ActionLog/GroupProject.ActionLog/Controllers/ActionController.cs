﻿using GroupProject.ActionLog.Core.Requests;
using GroupProject.ActionLog.Infrastructure.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GroupProject.ActionLog.Controllers
{
    [TypeFilter(typeof(ApiExceptionFilter))]
    [Route("v1/log/[controller]")]
    public class ActionController : ControllerBase
    {
        IMediator _mediator;

        public ActionController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetTopics([FromQuery] GetActionsRequest request)
        {
            var result = await _mediator.Send(request);
            return Ok(result);
        }

        [HttpPost("{userId}")]
        public async Task<IActionResult> AddTopic([FromBody] AddActionRequest request, string userId)
        {
            request.UserId = userId;

            var result = await _mediator.Send(request);
            return Ok(result);
        }
    }
}
