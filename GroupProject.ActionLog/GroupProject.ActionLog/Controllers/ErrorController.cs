﻿using GroupProject.ActionLog.Core.Responses;
using GroupProject.ActionLog.Infrastructure.Exceptions;
using GroupProject.ActionLog.Infrastructure.Utility;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System;

namespace GroupProject.ActionLog.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorsController : ControllerBase
    {
        [Route("error")]
        public ErrorResponse Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context?.Error;

            var response = CreateDefaultErrorResponse(exception);
            Response.StatusCode = 500;

            if (exception is ApiException)
            {
                var apiException = exception as ApiException;

                response.Code = apiException.Code;
                response.Error = apiException.Message;
                response.Details = apiException.Details;

                Response.StatusCode = 400;
            }

            return response;
        }

        private ErrorResponse CreateDefaultErrorResponse(Exception exception)
        {
            return new ErrorResponse()
            {
                Code = "1000",
                Error = "Internal error",
                Details = exception.Message,
                StackTrace = EnvironmentState.IsDevelopment ? exception.StackTrace : ""
            };
        }
    }
}
