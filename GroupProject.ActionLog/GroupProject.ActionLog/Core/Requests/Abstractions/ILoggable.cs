﻿namespace GroupProject.ActionLog.Core.Requests.Abstractions
{
    public interface ILoggable
    {
        public string ToLog();
    }
}
