﻿using GroupProject.ActionLog.Core.Requests.Abstractions;
using GroupProject.ActionLog.Core.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;

namespace GroupProject.ActionLog.Core.Requests
{
    public class GetActionsRequest : IRequest<GetActionsResponse>, ILoggable
    {
        [BindNever]
        public int LimitValue
        {
            get => int.Parse(Limit);
            set => throw new NotSupportedException();
        }
        public string Limit { get; set; }

        [BindNever]
        public int OffsetValue
        {
            get => int.Parse(Offset);
            set => throw new NotSupportedException();
        }
        public string Offset { get; set; }

        public string UserId { get; set; }

        [BindNever]
        public DateTime IssuedAfterValue
        {
            get => DateTime.Parse(IssuedAfter);
            set => throw new NotSupportedException();
        }
        public string IssuedAfter { get; set; }

        [BindNever]
        public DateTime IssuedBeforeValue
        {
            get => DateTime.Parse(IssuedBefore);
            set => throw new NotSupportedException();
        }
        public string IssuedBefore { get; set; }

        public string ToLog()
        {
            return $"{nameof(GetActionsRequest)} - offset:{Offset}, limit:{Limit}, userId:{UserId}, issuedBefore:{IssuedBefore}, issuedAfter:{IssuedAfter}";
        }
    }
}
