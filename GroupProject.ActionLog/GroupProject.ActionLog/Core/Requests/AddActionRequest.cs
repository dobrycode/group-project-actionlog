﻿using GroupProject.ActionLog.Core.Requests.Abstractions;
using GroupProject.ActionLog.Core.Responses;
using MediatR;

namespace GroupProject.ActionLog.Core.Requests
{
    public class AddActionRequest : IRequest<AddActionResponse>, ILoggable
    {
        public string ApplicationId { get; set; }
        public string TopicId { get; set; }
        public string Comment { get; set; }
        public string UserId { get; set; }

        public string ToLog()
        {
            return $"{nameof(AddActionRequest)} - applicationId:{ApplicationId}, topicId:{TopicId}, comment:{Comment}";
        }
    }
}
