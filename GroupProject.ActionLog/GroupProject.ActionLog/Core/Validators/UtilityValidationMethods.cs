﻿using System;
using System.Text.RegularExpressions;

namespace GroupProject.ActionLog.Core.Validators
{
    public class UtilityValidationMethods
    {
        public static bool IsNumber(string str)
        {
            int result;
            return str != null && int.TryParse(str, out result) && result >= 0;
        }

        public static bool IsValidName(string str)
        {
            return str != null && Regex.IsMatch(str, @"^[a-zA-Z_]{5,30}$");
        }

        public static bool IsValidNamePart(string str)
        {
            return str != null && Regex.IsMatch(str, @"^[a-zA-Z_]{1,30}$");
        }

        public static bool IsGuid(string str)
        {
            return str != null && Guid.TryParse(str, out Guid guid);
        }

        public static bool IsValidComment(string str)
        {
            return str != null && str.Length < 255;
        }

        public static bool IsValidDateTime(string str)
        {
            return str != null && DateTime.TryParse(str, out DateTime x);
        }
    }
}

