﻿using FluentValidation;
using GroupProject.ActionLog.Core.Requests;

namespace GroupProject.ActionLog.Core.Validators
{
    public class GetActionsRequestValidator : AbstractValidator<GetActionsRequest>
    {
        public GetActionsRequestValidator()
        {
            RuleFor(x => x.UserId)
                .NotNull().NotEmpty()
                .Must(x => UtilityValidationMethods.IsGuid(x));

            RuleFor(x => x.Limit)
                .Must(x => UtilityValidationMethods.IsNumber(x))
                .When(x => x.Limit != null && x.Limit.Length != 0);

            RuleFor(x => x.Offset)
                .Must(x => UtilityValidationMethods.IsNumber(x))
                .When(x => x.Offset != null && x.Offset.Length != 0);

            RuleFor(x => x.IssuedAfter)
                .Must(x => UtilityValidationMethods.IsValidDateTime(x))
                .When(x => x.Offset != null && x.Offset.Length != 0);

            RuleFor(x => x.IssuedBefore)
                .Must(x => UtilityValidationMethods.IsValidDateTime(x))
                .When(x => x.Offset != null && x.Offset.Length != 0);


        }
    }
}
