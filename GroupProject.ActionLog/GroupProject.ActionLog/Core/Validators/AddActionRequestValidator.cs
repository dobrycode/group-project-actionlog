﻿using FluentValidation;
using GroupProject.ActionLog.Core.Requests;
using GroupProject.ActionLog.Core.Services;

namespace GroupProject.ActionLog.Core.Validators
{
    public class AddActionRequestValidator : AbstractValidator<AddActionRequest>
    {
        private readonly TopicValidatorService _topicValidator;

        public AddActionRequestValidator(TopicValidatorService topicValidator)
        {
            _topicValidator = topicValidator;

            RuleFor(x => x.Comment)
                .NotNull().NotEmpty()
                .Must(x => UtilityValidationMethods.IsValidComment(x));

            RuleFor(x => x.TopicId)
                .NotNull().NotEmpty()
                .Must(x => UtilityValidationMethods.IsGuid(x) && _topicValidator.IsTopicValid(x));

            RuleFor(x => x.ApplicationId)
                .NotNull().NotEmpty()
                .Must(x => UtilityValidationMethods.IsGuid(x));

            RuleFor(x => x.UserId)
                .NotNull().NotEmpty()
                .Must(x => UtilityValidationMethods.IsGuid(x));
        }
    }
}
