﻿using GroupProject.ActionLog.Infrastructure.Exceptions;
using System.Net;
using System.Net.Http;

namespace GroupProject.ActionLog.Core.Services
{
    public class TopicValidatorService
    {
        private readonly IHttpClientFactory _clientFactory;

        public TopicValidatorService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public bool IsTopicValid(string topicId)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"http://host.docker.internal:5000/v1/log/topic/{topicId}");

            var client = _clientFactory.CreateClient();

            var response = client.SendAsync(request).GetAwaiter().GetResult();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                return false;
            }

            throw new ApiException("500", "Unable to validate topic", $"Received status code {(int)response.StatusCode}");
        }
    }
}
