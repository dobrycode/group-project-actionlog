﻿using Microsoft.EntityFrameworkCore;

namespace GroupProject.ActionLog.Core.Data
{
    public class ActionDbContext : DbContext
    {
        public ActionDbContext(DbContextOptions<ActionDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<ActionDbModel> Actions { get; set; }
    }
}
