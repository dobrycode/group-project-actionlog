﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GroupProject.ActionLog.Core.Data
{
    public class ActionDbModel
    {
        public int Id { get; set; }
        public Guid LogId { get; set; }
        public Guid UserId { get; set; }
        public Guid ApplicationId { get; set; }
        public DateTime IssuedAt { get; set; }
        public Guid TopicId { get; set; }

        [MaxLength(256)]
        public string Comment { get; set; }
    }
}
