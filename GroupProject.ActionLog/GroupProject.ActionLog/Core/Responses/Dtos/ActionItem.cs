﻿using System;

namespace GroupProject.ActionLog.Core.Responses.Dtos
{
    public class ActionItem
    {
        public Guid LogId { get; set; }
        public Guid UserId { get; set; }
        public Guid ApplicationId { get; set; }
        public DateTime IssuedAt { get; set; }
        public Guid TopicId { get; set; }
        public string Comment { get; set; }

    }
}
