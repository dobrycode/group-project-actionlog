﻿using GroupProject.ActionLog.Core.Responses.Dtos;
using System.Collections.Generic;

namespace GroupProject.ActionLog.Core.Responses
{
    public class GetActionsResponse
    {
        public IEnumerable<ActionItem> Logs { get; set; }
    }
}
