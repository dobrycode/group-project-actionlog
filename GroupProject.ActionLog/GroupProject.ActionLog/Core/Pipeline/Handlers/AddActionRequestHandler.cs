﻿using GroupProject.ActionLog.Core.Data;
using GroupProject.ActionLog.Core.Requests;
using GroupProject.ActionLog.Core.Responses;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GroupProject.ActionLog.Core.Pipeline.Handlers
{
    public class AddActionRequestHandler : IRequestHandler<AddActionRequest, AddActionResponse>
    {
        private readonly ActionDbContext _context;

        public AddActionRequestHandler(ActionDbContext context)
        {
            _context = context;
        }

        public async Task<AddActionResponse> Handle(AddActionRequest request, CancellationToken cancellationToken)
        {
            var action = new ActionDbModel()
            {
                LogId = Guid.NewGuid(),
                UserId = new Guid(request.UserId),
                ApplicationId = new Guid(request.ApplicationId),
                TopicId = new Guid(request.TopicId),
                IssuedAt = DateTime.Now,
                Comment = request.Comment
            };

            _context.Actions.Add(action);

            await _context.SaveChangesAsync();

            return new AddActionResponse()
            {
                LogId = action.LogId,
                UserId = action.UserId,
                ApplicationId = action.ApplicationId,
                TopicId = action.TopicId,
                IssuedAt = action.IssuedAt,
                Comment = action.Comment
            };
        }
    }
}
