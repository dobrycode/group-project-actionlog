﻿using GroupProject.ActionLog.Core.Data;
using GroupProject.ActionLog.Core.Requests;
using GroupProject.ActionLog.Core.Responses;
using GroupProject.ActionLog.Core.Responses.Dtos;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GroupProject.ActionLog.Core.Pipeline.Handlers
{
    public class GetActionsRequestHandler : IRequestHandler<GetActionsRequest, GetActionsResponse>
    {
        private readonly ActionDbContext _context;

        public GetActionsRequestHandler(ActionDbContext context)
        {
            _context = context;
        }

        public async Task<GetActionsResponse> Handle(GetActionsRequest request, CancellationToken cancellationToken)
        {
            if (request.Limit == null || request.Limit.Length == 0)
            {
                request.Limit = "20";
            }

            if (request.Offset == null || request.Offset.Length == 0)
            {
                request.Offset = "0";
            }

            var query = _context.Actions.Select(x => new ActionItem()
            {
                LogId = x.LogId,
                TopicId = x.TopicId,
                ApplicationId = x.ApplicationId,
                UserId = x.UserId,
                Comment = x.Comment,
                IssuedAt = x.IssuedAt
            }
            );

            if (request.IssuedBefore != null && request.IssuedBefore.Length != 0)
            {
                query = query.Where(x => x.IssuedAt <= request.IssuedBeforeValue);
            }

            if (request.IssuedAfter != null && request.IssuedAfter.Length != 0)
            {
                query = query.Where(x => x.IssuedAt >= request.IssuedAfterValue);
            }

            query = query.Skip(request.OffsetValue).Take(request.LimitValue);

            return new GetActionsResponse()
            {
                Logs = query.AsEnumerable()
            };
        }
    }
}
