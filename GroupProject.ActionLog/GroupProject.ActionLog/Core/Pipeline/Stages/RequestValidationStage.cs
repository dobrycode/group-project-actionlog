﻿using FluentValidation;
using GroupProject.ActionLog.Infrastructure.Exceptions;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GroupProject.ActionLog.Core.Pipeline.Stages
{
    public class RequestValidationStage<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        AbstractValidator<TRequest> _validator;

        public RequestValidationStage(AbstractValidator<TRequest> validator)
        {
            _validator = validator;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var result = await _validator.ValidateAsync(request);
            if (!result.IsValid)
            {
                var error = result.Errors.First();
                throw new ApiException(error.ErrorCode, error.ErrorMessage, "");
            }

            return await next();
        }
    }
}
