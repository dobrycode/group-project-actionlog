﻿using GroupProject.ActionLog.Core.Requests.Abstractions;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace GroupProject.ActionLog.Core.Pipeline.Stages
{
    public class RequestLoggingStage<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private ILogger<RequestLoggingStage<TRequest, TResponse>> _logger;
        public RequestLoggingStage(ILogger<RequestLoggingStage<TRequest, TResponse>> logger)
        {
            _logger = logger;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            if (request is ILoggable loggable)
            {
                _logger.LogInformation($"Processing request: {loggable.ToLog()}");
            }
            else
            {
                _logger.LogWarning($"Processing non-loggable request");
            }

            return await next();
        }
    }
}
