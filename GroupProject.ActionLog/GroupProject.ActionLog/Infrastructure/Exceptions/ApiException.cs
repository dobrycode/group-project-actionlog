﻿using System;

namespace GroupProject.ActionLog.Infrastructure.Exceptions
{
    public class ApiException : Exception
    {
        public ApiException(string code, string message, string details)
            : base(message)
        {
            Code = code;
            Details = details;
        }

        public string Code { get; set; }
        public string Details { get; set; }
    }
}
