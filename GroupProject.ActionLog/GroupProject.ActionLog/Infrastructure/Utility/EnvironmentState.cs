﻿using System;

namespace GroupProject.ActionLog.Infrastructure.Utility
{
    public class EnvironmentState
    {
        public static bool IsDevelopment => Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";

    }
}
