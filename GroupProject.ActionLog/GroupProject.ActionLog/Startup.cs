using FluentValidation;
using GroupProject.ActionLog.Core.Data;
using GroupProject.ActionLog.Core.Pipeline.Stages;
using GroupProject.ActionLog.Core.Requests;
using GroupProject.ActionLog.Core.Services;
using GroupProject.ActionLog.Core.Validators;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace GroupProject.ActionLog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddMediatR(Assembly.GetExecutingAssembly());

            services.AddHttpClient<TopicValidatorService>();
            services.AddTransient<TopicValidatorService>();

            services.AddTransient<AbstractValidator<AddActionRequest>, AddActionRequestValidator>();
            services.AddTransient<AbstractValidator<GetActionsRequest>, GetActionsRequestValidator>();

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestLoggingStage<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationStage<,>));

            services.AddDbContext<ActionDbContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("MySqlConnection"))
            );
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseExceptionHandler("/error");

            app.UseRouting();

            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
